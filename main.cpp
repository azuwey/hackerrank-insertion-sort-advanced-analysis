/**
 * Author: Zarandi David (Azuwey)
 * Date: 02/23/2019
 * Source: HackerRank - https://www.hackerrank.com/challenges/insertion-sort/problem
 **/
#include <bits/stdc++.h>

using namespace std;

vector<string> split_string(string);

class BTree {
 public:
  BTree(const long int &ss) {
    tree = new int[ss];
    s = ss;
  }

  ~BTree() { delete[] tree; }

  void update(int idx, int val) {
    while (idx <= s) {
      tree[idx] += val;
      idx += (idx & -idx);
    }
  }

  unsigned long long read(int idx) {
    unsigned long long sum = 0;
    while (idx > 0) {
      sum += tree[idx];
      idx -= (idx & -idx);
    }
    return sum;
  }

 private:
  long int s;
  int *tree;
};

unsigned long long insertionSort(vector<int> arr) {
  BTree *bit = new BTree(20000002);
  unsigned long long s = 0;
  int ss = arr.size();
  for (int i = ss - 1; i >= 0; --i) {
    bit->update(arr[i], 1);
    s += bit->read(arr[i] - 1);
  }
  delete bit;
  return s;
}

int main() {
  ofstream fout(getenv("OUTPUT_PATH"));

  int t;
  cin >> t;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');

  for (int t_itr = 0; t_itr < t; t_itr++) {
    int n;
    cin >> n;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    string arr_temp_temp;
    getline(cin, arr_temp_temp);

    vector<string> arr_temp = split_string(arr_temp_temp);

    vector<int> arr(n);

    for (int i = 0; i < n; i++) {
      int arr_item = stoi(arr_temp[i]);

      arr[i] = arr_item;
    }

    unsigned long long result = insertionSort(arr);

    fout << result << "\n";
  }

  fout.close();

  return 0;
}

vector<string> split_string(string input_string) {
  string::iterator new_end =
      unique(input_string.begin(), input_string.end(),
             [](const char &x, const char &y) { return x == y and x == ' '; });

  input_string.erase(new_end, input_string.end());

  while (input_string[input_string.length() - 1] == ' ') {
    input_string.pop_back();
  }

  vector<string> splits;
  char delimiter = ' ';

  size_t i = 0;
  size_t pos = input_string.find(delimiter);

  while (pos != string::npos) {
    splits.push_back(input_string.substr(i, pos - i));

    i = pos + 1;
    pos = input_string.find(delimiter, i);
  }

  splits.push_back(
      input_string.substr(i, min(pos, input_string.length()) - i + 1));

  return splits;
}
